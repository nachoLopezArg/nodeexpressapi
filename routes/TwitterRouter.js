import express from 'express';
import { TwitterApiService } from '../services/TwitterApiService.js';
import {Trends} from '../models/mongo/Trends.js';
export const TwitterRouter = express.Router();
/* GET users listing. */
TwitterRouter
  .get('/twitter/:user/timeline', async (req, res, next) => {
    try {
      const o = await TwitterApiService.getUserTimeLine(
        { token: req.session.user.twitter.token, tokenSecret: req.session.user.twitter.tokenSecret }, {
        'screen_name': req.params.user || req.session.user.twitter.screen_name,
        //incluyo Retweets por defecto
        'include_rts': req.query.rt || true,
        'count': req.query.count || 200
      });
      res.send(o);
    } catch (error) {
      console.log(error);
      next(error);
    }
  })
  .get('/twitter/:user/timeline/users', async (req, res, next) => {
    try {
      const o = await TwitterApiService.getUserTimeLineUsers(
        { token: req.session.user.twitter.token, tokenSecret: req.session.user.twitter.tokenSecret }, {
        //actual username
        'screen_name': req.params.user || req.session.user.twitter.screen_name,
        //incluyo Retweets por defecto
        'include_rts': req.query.rt || true,
        'count': req.query.count || 200
      });
      res.send(o);
    } catch (error) {
      console.log(error);
      next(error);
    }
  })
  .get('/twitter/trends', async (req, res, next) => {
    try {
      res.send(await TwitterApiService.getTrends({ token: req.session.user.twitter.token, tokenSecret: req.session.user.twitter.tokenSecret }, req.query.whoeid));
    } catch (error) {
      console.log(error);
      next(error);
    }
  })
  .get('/twitter/trends/places', async (req, res, next) => {
    try {
      res.send(await TwitterApiService.getTrendsPlaces({ token: req.session.user.twitter.token, tokenSecret: req.session.user.twitter.tokenSecret }));
    } catch (error) {
      console.log(error);
      next(error);
    }
  })
  .get('/twitter/geo', async (req, res, next) => {
    try {
      res.send(await TwitterApiService.searchLocationByIP({ token: req.session.user.twitter.token, tokenSecret: req.session.user.twitter.tokenSecret }, {
        'lat': req.query.lat,
        'long': req.query.long,
        'ip': req.query.ip,
        'query': req.query.q,
        'accuracy': req.query.radio
      }));
    } catch (error) {
      console.log(error);
      next(error);
    }
  })
  .get('/twitter/search', async (req, res, next) => {
    try {
      let max_id = undefined;
      let total = 0;
      const started = new Date();
      let ends = null;
      do {
        const tweets = await TwitterApiService.search({ token: req.session.user.twitter.token, tokenSecret: req.session.user.twitter.tokenSecret }, {
          'q': req.query.q,
          'max_id': max_id,
          'count': '100'
        });
        for (let index = 0; index < tweets.statuses.length; index++) {
          const tweet = tweets.statuses[index];
          Trends.findOneAndUpdate({hashtag:decodeURI(req.query.q)}, { tweet: tweet },function(error, result) {
            if (!error) {
                // If the document doesn't exist      
                Trends.create({hashtag:decodeURI(req.query.q),tweet:tweet},(error,doc)=> {
                    if (!error) {
                        // Do something with the document
                    } else {
                        throw error;
                    }
                });
            }})
        }
        total+=100;
        console.log('Recolecting tweets. total:',total,' max_id:', tweets.search_metadata.max_id);
        max_id = tweets.search_metadata.next_results?JSON.parse('{"' + decodeURI(tweets.search_metadata.next_results).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}')['?max_id']:undefined;

      } while (max_id)
      ends=new Date();
      console.log(`Process started at ${started} and ends at ${ends}`)

      res.send('ok');
 
    } catch (error) {
      console.log(error);
      next(error);
    }
  })


