import express from 'express';
import {CurriculumService} from '../services/CurriculumService.js';

export const CurriculumRouter = express.Router();
/* GET users listing. */
CurriculumRouter.route('/cv')
  .get(async (req,res, next) => {
    try {
      const curriculum = await CurriculumService.getCurriculum();
      res.send(curriculum);
    } catch (error) {
      console.log(error);
      next(error);
    }
  })
  .post((req, res, next) => {
    try{
      res.send(CurriculumService.create(req));
    }catch(error){
      console.log(error);
      next(error);
    }
  })
  .patch((req,res,next)=>{

  });

