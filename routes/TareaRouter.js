import express from 'express';
import {TareaService} from '../services/TareaService.js';

export const TareaRouter = express.Router();
/* GET users listing. */
TareaRouter.route('/cv')
  .get(async (req,res, next) => {
    try {
      const o = await TareaService.getCurriculum();
      res.send(o);
    } catch (error) {
      console.log(error);
      next(error);
    }
  })
  .post((req, res, next) => {
    try{
      res.send(TareaService.create(req));
    }catch(error){
      console.log(error);
      next(error);
    }
  })
  .patch((req,res,next)=>{

  });

