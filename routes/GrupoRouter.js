import express from 'express';
import { GrupoService } from '../services/GrupoService.js';

export const GrupoRouter = express.Router();
const RESOURCE = '/grupos';
/* GET users listing. */
GrupoRouter
  .delete(RESOURCE, async (req, res, next) => {
    try {
      res.send(await GrupoService.deleteGrupo(req.params.id));
    } catch (error) {
      console.log(error);
      next(error);
    }
  })
  .get(`${RESOURCE}/:id`,
    /**
    * @swagger
    * /grupos/{id}:
    *   get:
    *     description: Get groups by id if authenticated user has permissions. 
    *     parameters:
    *       - in: path
    *         name: id
    *         type: string
    *     produces:
    *       - application/json
    *     responses:
    *       200:
    *         description: Groups collection
    *         schema:
    *           type: array
    *           items:
    *           $ref: '#/definitions/Grupo'
    */
    async (req, res, next) => {
      try {
        const o = await GrupoService.getGrupoById(req.params.id);
        res.send(o);
      } catch (error) {
        console.log(error);
        next(error);
      }
    })

  /**
  * @swagger
  * /grupos:
  *   get:
  *     description: Get authenticaded user groups.
  *     produces:
  *       - application/json
  *     responses:
  *       200:
  *         description: Groups collection
  *         schema:
  *           type: array
  *           items:
  *             $ref: '#/definitions/Grupo'
  */
  .get(RESOURCE, async (req, res, next) => {
    try {
      const o = await GrupoService.getGrupos();
      res.send(o);
    } catch (error) {
      console.log(error);
      next(error);
    }
  })
  /**
  * @swagger
  * /grupos:
  *   post:
  *     description: Create group. 
  *     consumes:
  *       - application/json
  *     parameters:
  *       - in: body
  *         name: grupo
  *         description: The group to create.
  *         schema:
  *           $ref: '#/definitions/Grupo' 
  *     produces:
  *       - application/json
  *     responses:
  *       200:
  *         description: Created group
  *         schema:
  *           type: object
  *           $ref: '#/definitions/Grupo'
  */
  .post(RESOURCE, async (req, res, next) => {
    try {
      res.send(await GrupoService.createGrupo(req.body,req.session.user));
    } catch (error) {
      console.log(error);
      next(error);
    }
  })
  .patch(RESOURCE, async (req, res, next) => {
    try {
      res.send(await GrupoService.updateGrupo(req));
    } catch (error) {
      console.log(error);
      next(error);
    }
  })


