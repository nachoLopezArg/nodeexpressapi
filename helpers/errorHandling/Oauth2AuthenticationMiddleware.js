import passport from 'passport';
import googleOauth from 'passport-google-oauth';
const GoogleStrategy = googleOauth.OAuth2Strategy;
import twitterOauth from 'passport-twitter';
const TwitterStrategy = twitterOauth.Strategy;
import express from 'express';
import { env } from '../../bin/env.js';
import { UnauthorizedException } from '../../models/exceptions/http/UnauthorizedException.js';
import { UserService } from '../../services/UserService.js'
export const passportRouter = express.Router();
export const myPassport = passport;

export const isAuthenticated = async function (req, res, next) {
  console.log(1);
  try {
    if (req.session.user) {
      return next();
    }
    else {
      throw new UnauthorizedException('NotAuthorizedException');
    }
  } catch (error) {
    next(error);
  }
}

//oauth2 twitter config
myPassport.use(new TwitterStrategy({
  consumerKey: env.TWITTER_CONSUMER_KEY,
  consumerSecret: env.TWITTER_CONSUMER_SECRET,
  callbackURL: env.TWITTER_CALLBACK_URL
},
  function (token, tokenSecret, profile, done) {
    let twitter = profile._json;
    twitter.token = token;
    twitter.tokenSecret = tokenSecret;
    done(null, { twitter: twitter });

  }
));

//oauth2 google config
myPassport.use(new GoogleStrategy({
  clientID: env.GOOGLE_OAUTH_CLIENT_ID,
  clientSecret: env.GOOGLE_OAUTH_SECRET,
  callbackURL: env.GOOGLE_OAUTH_CALLBACK_URI
},
  function (accessToken, refreshToken, profile, done) {
    done(null, { google: profile._json, accessToken: accessToken, refreshToken: refreshToken });
  }
));

//routes handling 

//routes twitter 
passportRouter.get(env.TWITTER_CALLBACK_URL, async function (req, res, next) {
  try {
    myPassport.authenticate('twitter', async function (err, user, info) {
      if (!user) { return res.redirect(env.PASSPORT_GOOGLE_LOGIN_PAGE); }
      else {
        if (await UserService.isNewUser(user)) {
          req.session.user = await UserService.createUser(user);
        } else {
          req.session.user = user;
        }
        return res.redirect('/docs')
      }
    })(req, res, next);
  } catch (error) {
    next(error);
  }

});
//google
passportRouter.get(env.PASSPORT_GOOGLE_CALLBACK_PAGE, async function (req, res, next) {
  try {
    myPassport.authenticate('google', async function (err, user, info) {
      if (!user) { return res.redirect(env.PASSPORT_GOOGLE_LOGIN_PAGE); }
      else {
        if (await UserService.isNewUser(user)) {
          req.session.user = await UserService.createUser(user);
        } else {
          req.session.user = user;
        }
        return res.redirect('/docs')
      }
    })(req, res, next);
  } catch (error) {
    next(error);
  }

});

passportRouter.use(env.PASSPORT_GOOGLE_LOGIN_PAGE, myPassport.authenticate('google', {
  scope: ['profile', 'email']
}))
passportRouter.use(env.PASSPORT_TWITTER_LOGIN_PAGE, myPassport.authenticate('twitter'));
