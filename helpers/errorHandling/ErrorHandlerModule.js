export function errorHandlerMiddleware(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    if (!(err instanceof HandledError)) {
        err = new HandledError('Not found', undefined, 404);
    }
    res.status(err.code);
    res.send({
        'msg': err.msg,
        'cause': err.cause,
        'code': err.code
    })
    //res.render('error');
}
export class HandledError {
    constructor(msg, cause, code) {
        this.msg = msg;
        this.cause = cause || undefined;
        this.code = code || 500;
    }
}
