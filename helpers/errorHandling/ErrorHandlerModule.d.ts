
export declare function errorHandlerMiddleware(err:HandledError, req, res, next) ;
declare class HandledError{
    constructor(msg:String,cause?:String,code?:number);
    msg : String;
    cause?:String;
    //statusCode || 500
    code?:number;
}
export = {HandledError,errorHandlerMiddleware};
