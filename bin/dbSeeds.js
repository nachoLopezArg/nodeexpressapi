import { Curriculum } from '../models/mongo/Curriculum.js';
import {CurriculumService} from '../services/CurriculumService.js';
export const initSeeds = async () => {
    if(! await CurriculumService.existsCurriculumByEmail('igloar96@gmail.com')){
        await new Curriculum(
            {
                name: 'Ignacio',
                lastname: 'López',
                email: 'igloar96@gmail.com',
                birthdate: new Date(1996, 4, 26),
                objetivo: 'Crecer profesionalmente, acompañando las últimas tecnologías del mercado.',
                empleos: [{
                    company: 'Oracle - Puerto Madero',
                    positions: [{
                        position: 'Desarrollador de software',
                        dateFrom: new Date(),
                        dateTo: new Date()
                    }],
                }]
            }
        ).save();
    
    
    }
        console.log("Seeds saved to Db :) ");

}