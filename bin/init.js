import { mongoConn } from './dbConn.js';
import { serverInit } from './www.js';
import { initSeeds} from './dbSeeds.js';
import {env} from './env.js';
console.log(`Trying to connect to mongo : ${env.MONGO_DB_HOST}:${env.MONGO_DB_PORT}/${env.MONGO_DB_DATABASE}`);

mongoConn
  .then(async () => {
    await initSeeds();
    serverInit();
  }).catch((error) => {
    console.log("No se puede conectar con MongoDb");
    console.debug(error);
    process.exit();
  });




