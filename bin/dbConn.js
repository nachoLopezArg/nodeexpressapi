// Cargamos el módulo de mongoose para poder conectarnos a MongoDB
import mongoose from 'mongoose';
import {env} from './env.js';

// Usamos el método connect para conectarnos a nuestra base de datos

const connStr = `mongodb://${env.MONGO_DB_USERNAME}:${env.MONGO_DB_PASSWORD}@${env.MONGO_DB_HOST}:${env.MONGO_DB_PORT}/${env.MONGO_DB_DATABASE}`;
export const mongoConn = mongoose.connect(connStr, {
        useNewUrlParser: true, useUnifiedTopology: true
    });


