'use strict'
import mongoose from 'mongoose';
// Cargamos el módulo de mongoose
// Usaremos los esquemas
const Schema = mongoose.Schema;
// Creamos el objeto del esquema y sus atributos

const TrendsSchema = Schema({
  hashtag: { type: String, required: true },
  tweet: {   
  "created_at": Date,
  "id": Number,
  "id_str": String,
  "text": String,
  "truncated": Boolean,
  "entities": Object,
  "metadata": Object,
  "source": String,
  "in_reply_to_status_id": Number,
  "in_reply_to_status_id_str": String,
  "in_reply_to_user_id": Number,
  "in_reply_to_user_id_str": String,
  "in_reply_to_screen_name": String,
  "user": Object,
  "geo": Object,
  "coordinates": Object,
  "place": Object,
  "contributors": Object,
  "is_quote_status": Boolean,
  "retweet_count": Number,
  "favorite_count": Number,
  "favorited": Boolean,
  "retweeted": Boolean,
  "lang": String}
});
TrendsSchema.index({"tweet.created_at": -1});


export const Trends = mongoose.model('Trends', TrendsSchema);