'use strict'
import mongoose from 'mongoose';
// Cargamos el módulo de mongoose
// Usaremos los esquemas
const Schema = mongoose.Schema;
// Creamos el objeto del esquema y sus atributos

const UserSchema = Schema({
  method: {
    type: String,
    enum: ['local', 'google'],
  },
  local: {
    email: {
      type: String,
      lowercase: true
    },
    password: {
      type: String
    }
  },
  twitter: {
    id: { type: Number },
    id_str: { type: String },
    name: { type: String },
    screen_name: { type: String },
    location: { type: String },
    description: { type: String },
    url: { type: String },
    protected: { type: Boolean },
    followers_count: { type: Number },
    friends_count: { type: Number },
    created_at: { type: String },
    favourites_count: { type: String },
    verified: { type: Boolean },
    lang: { type: String },
    token: {type: String},
    tokenSecret : {type:String}
  },
  google: {
    id: {
      type: String
    },
    sub: { type: String },
    name: { type: String },
    given_name: { type: String },
    family_name: { type: String },
    email: {
      type: String,
      lowecase: true
    },
    picture: {
      type: String,
      lowercase: true
    },
    email_verified: { type: Boolean },
    locale: { type: String }
  }
});

export const User = mongoose.model('User', UserSchema);