'use strict'
import mongoose from 'mongoose';
// Cargamos el módulo de mongoose
// Usaremos los esquemas
const Schema = mongoose.Schema;
// Creamos el objeto del esquema y sus atributos
/**
 * @swagger
 *
 * definitions:
 *  Grupo:
 *    type: object
 *    required:
 *      - nombre
 *    properties:
 *      nombre:
 *        type: string
 *      descripcion:
 *        type: string
 *      analisis:
 *        type: array
 *        items:
 *        $ref: '#/definitions/Analisis'
 *  Analisis:
 *    type: object
 *    required:
 *      - nombre
 *    properties:
 *      nombre:
 *        type: string
 *      descripcion:
 *        type: string
 *        
 */
const GrupoSchema = Schema({
  nombre: { type: String, required: true },
  descripcion: String,
  analisis: [{
    nombre: { type: String, required: true },
    descripcion: String,
  }],
  usuarios: [
    { type: Schema.ObjectId, ref: 'User' }
  ]
});


export const Grupo = mongoose.model('Grupo', GrupoSchema);