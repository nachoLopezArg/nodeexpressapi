import { Model } from "mongoose";


export interface HashtagStudy extends Document,Model<HashtagStudy> {
    nombre: { type: String, required: true },
    description: String,
    steps: [{
        nombre: { type: String, required: true },
        descripcion: String,
        done: Boolean
    }],
    categorias: [
        { type: Schema.ObjectId, ref: 'Categoria' }
    ]
}