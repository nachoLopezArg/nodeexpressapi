'use strict'
import mongoose from 'mongoose';
// Cargamos el módulo de mongoose
// Usaremos los esquemas
const Schema = mongoose.Schema;
// Creamos el objeto del esquema y sus atributos

const HashtagStudySchema = Schema({
  hashtag: { type: String, required: true },
  total: { type: number },
  users_count: { type: number },
  analisis_id: { type: Schema.ObjectId, ref: 'Analisis' },
  group_country:[{
    country: {type:String,required:true},
    users_count:{type:String,required:true},
    total_count:{type:String,required:true}
  }],
  //por defecto top 10
  group_users_country:[
    {
      twitter_id:{type:number,required:true},
      total : {type:number,required:true}
    }
  ]
});
export const Tarea = mongoose.model('HashtagStudy', HashtagStudySchema);
