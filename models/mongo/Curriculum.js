'use strict'
import mongoose from 'mongoose';
// Cargamos el módulo de mongoose
// Usaremos los esquemas
const Schema = mongoose.Schema;
// Creamos el objeto del esquema y sus atributos


const CurriculumSchema = Schema({
    name: String,
    lastname: String,
    email: String,
    birthdate: Date,
    objetivo: String,
    empleos: [{
        company: String,
        positions: [{
            position: String,
            dateFrom: Date,
            dateTo: Date
        }],
    }],
    owners: [
        { type: Schema.ObjectId, ref: 'User' }
    ],
    comments: [{
        comment: String,
        owner: { type: Schema.ObjectId, ref: 'User' }
    }]
});

export const Curriculum = mongoose.model('Curriculum', CurriculumSchema);