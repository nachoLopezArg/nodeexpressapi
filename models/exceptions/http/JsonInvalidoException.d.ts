import HandledError = require("../../../helpers/errorHandling/ErrorHandlerModule");

declare class JsonInvalidoException extends HandledError{
    constructor(msg:String,cause?:String,code?:number){
        super(msg,cause,code);
    }
}

export {JsonInvalidoException}