import {HandledError} from '../../../helpers/errorHandling/ErrorHandlerModule.js';

export class DuplicatedException extends HandledError {
    constructor(msg, cause, code) {
        super(msg, cause, code || 400)
    }
}