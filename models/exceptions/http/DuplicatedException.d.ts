import { HandledError } from "../../../helpers/errorHandling/ErrorHandlerModule";


declare class DuplicatedException extends HandledError{
    /**
     * 
     * @param msg Motivo por el cual esta duplicado
     * @param cause Causa que lo provoca detallada.
     * @param code Por defecto es HTTP 400 puede cambiarse
     */
    constructor(msg:String,cause?:String,code?:number){
        super(msg,cause,code||400);
    }
}

export {DuplicatedException}