import {HandledError} from '../../../helpers/errorHandling/ErrorHandlerModule.js';

export class UnauthorizedException extends HandledError{
    constructor(msg,cause,code){
        super(msg,cause,code||401);
    }
}

