import { HandledError } from "../../../helpers/errorHandling/ErrorHandlerModule";

declare class InexistenteException extends HandledError{
    constructor(msg:String,cause?:String,code?:number){
        super(msg,cause,code);
    }
}

export {InexistenteException}