import { HandledError } from "../../../helpers/errorHandling/ErrorHandlerModule.js";

export class InexistenteException extends HandledError {
    constructor(msg, cause, code) {
      super(msg,cause,code||404)
    }
}