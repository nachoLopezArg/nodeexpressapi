import {HandledError} from '../../../helpers/errorHandling/ErrorHandlerModule.js';

export class JsonInvalidoException extends HandledError{
    constructor(msg,cause,code){
        super(msg,cause,code);
    }
}

