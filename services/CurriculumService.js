import { Curriculum } from '../models/mongo/Curriculum.js'

export const CurriculumService = {
    existsCurriculumByEmail: (email) => {
        return new Promise((res, reject) => {
            try {
                Curriculum.findOne({email:email},(error, doc) => {
                    if(doc){
                        res(true);
                    }else{
                        res(false);
                    }
                })
            } catch (error) {
                reject(error);
            }
        });

    },
    getCurriculumById: (id) => {
        return new Promise((res, reject) => {
            try {
                Curriculum.findOne({_id:id},(error, doc) => {
                    res(doc);
                })
            } catch (error) {
                reject(error);
            }
        });

    },
    createCurriculum: (curriculum) => {
        return new Promise((res, reject) => {
            try {
                Curriculum.create(curriculum,(error, doc) => {
                    res(doc);
                })
            } catch (error) {
                reject(error);
            }
        });
    },
    updateCurriculum: (curriculum) => {
        return new Promise((res, reject) => {
            try {
                Curriculum.update(curriculum,(error, doc) => {
                    res(doc);
                })
            } catch (error) {
                reject(error);
            }
        });
    }
}