import fetch from 'node-fetch';
import Twitter from 'twitter';
import { env } from '../bin/env.js'
import { InexistenteException } from '../models/exceptions/http/InexistenteException.js';

const TWITTER_BASE_URL = 'https://api.twitter.com/1.1/';
const TWITTER_SEARCH_RESOURCE = 'search/tweets.json';
export const TwitterApiService = {

  getUserById: async (id,{ token, tokenSecret }) => {
    return new Promise((res,reject)=>{
      const client = new Twitter({
        consumer_key: env.TWITTER_CONSUMER_KEY,
        consumer_secret: env.TWITTER_CONSUMER_SECRET,
        access_token_key: token,
        access_token_secret: tokenSecret
      });
  
      var params = { user_id: id };
      client.get('users/show', params, function (error, tweets, response) {
        if (!error) {
          res(tweets);
        }else{
          reject(error);
        }
      });
    })
  },
  getUserTimeLineUsers: async ({ token, tokenSecret }, params) => {
    return new Promise((resolve, reject) => {
      const client = new Twitter({
        consumer_key: env.TWITTER_CONSUMER_KEY,
        consumer_secret: env.TWITTER_CONSUMER_SECRET,
        access_token_key: token,
        access_token_secret: tokenSecret
      });
      client.get('statuses/user_timeline', params, function (error, tweets, response) {
        if (!error) {
          const usersListJSON = tweets.map((t) => t.text).join(' ').match(/(?<=^|(?<=[^a-zA-Z0-9-_\.]))@([A-Za-z]+[A-Za-z0-9-_]+)/g);
          const usersCount = usersListJSON.reduce((a, c) => (a[c] = (a[c] || 0) + 1, a), Object.create(null));
          //resolve(usersCount);
          resolve(tweets);
        } else {
          reject(error);
        }
      });
    });
  },
  getUserTimeLine: async ({ token, tokenSecret }, params) => {
    return new Promise((resolve, reject) => {
      const client = new Twitter({
        consumer_key: env.TWITTER_CONSUMER_KEY,
        consumer_secret: env.TWITTER_CONSUMER_SECRET,
        access_token_key: token,
        access_token_secret: tokenSecret
      });
      //(?<=^|(?<=[^a-zA-Z0-9-_\.]))@([A-Za-z]+[A-Za-z0-9-_]+)\w+
      client.get('statuses/user_timeline', params, function (error, tweets, response) {
        if (!error) {
          const tweetsJSON = tweets.map((t) => t.text).join(' ');
          resolve(tweetsJSON);
        } else {
          reject(error);
        }
      });
    });

  },
  getTrendsPlaces:({token,tokenSecret})=>{
  
    return new Promise((res,reject)=>{
      const client = new Twitter({
        consumer_key: env.TWITTER_CONSUMER_KEY,
        consumer_secret: env.TWITTER_CONSUMER_SECRET,
        access_token_key: token,
        access_token_secret: tokenSecret
      });
  
      client.get('trends/available', {}, function (error, tweets, response) {
        if (!error) {
          res(tweets);
        }else{
          reject(error);
        }
      });
    })

  },
  getTrends : ({token,tokenSecret},whoeid)=>{
    return new Promise(async (resolve, reject) => {
      const client = new Twitter({
        consumer_key: env.TWITTER_CONSUMER_KEY,
        consumer_secret: env.TWITTER_CONSUMER_SECRET,
        access_token_key: token,
        access_token_secret: tokenSecret
      });

      client.get(`trends/place`, {id:whoeid}, function (error, tweets, response) {
        if (!error) {
          resolve(tweets);
        } else {
          reject(new InexistenteException(error[0].message,null,null));
        }
      });
    });
  },
  searchLocationByIP : ({ token, tokenSecret },params)=>{
    return new Promise((resolve, reject) => {
      const client = new Twitter({
        consumer_key: env.TWITTER_CONSUMER_KEY,
        consumer_secret: env.TWITTER_CONSUMER_SECRET,
        access_token_key: token,
        access_token_secret: tokenSecret
      });
      console.log(params);
      client.get('geo/search', params, function (error, tweets, response) {
        if (!error) {
          resolve(tweets);
        } else {
          reject(new InexistenteException(error[0].message,null,null));
        }
      });
    });
  },
  search : ({token,tokenSecret},params)=>{
    
    return new Promise(async (resolve, reject) => {
      const client = new Twitter({
        consumer_key: env.TWITTER_CONSUMER_KEY,
        consumer_secret: env.TWITTER_CONSUMER_SECRET,
        access_token_key: token,
        access_token_secret: tokenSecret
      });

      client.get(`search/tweets`,params, function (error, tweets, response) {
        if (!error) {
          resolve(tweets);
        } else {
          reject(new InexistenteException(error[0].message,null,null));
        }
      });
    });
  }





}