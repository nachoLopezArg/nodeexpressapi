import { User } from '../models/mongo/User.js'
import { InexistenteException } from '../models/exceptions/http/InexistenteException.js';

export const UserService = {
  getUserActual: () => {

  },
  getUserByEmail: (email) => {
    return new Promise((res, reject) => {
      User.findOne({ 'google.email': email }, (error, doc) => {
        if (doc) {
          res(doc)
        } else {
          reject(new InexistenteException(`No se encuentra el usuario con el email ${email}`));
        }
      })
    });
  },
  isNewUser: (user) => {
    return new Promise((res, reject) => {
      if (user.google) {
        User.findOne({ 'google.email': user.google.email }, (error, doc) => {
          if (doc) {
            res(false)
          } else {
            res(true);
          }
        })
      } else {
        User.findOne({ 'twitter.id': user.twitter.id }, (error, doc) => {
          if (doc) {
            res(false)
          } else {
            res(true);
          }
        })
      }
    });

  },
  createUser: (user) => {
    return new Promise((res, reject) => {
      User.create(user, (err, doc) => {
        res(doc);
      });
    });

  }
}