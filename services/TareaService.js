import Tarea  from '../models/mongo/Tarea.js'

export const TareaService = {

    createTarea: (tarea) => {
        return new Promise((res, reject) => {
            try{
                
                Tarea.create(tarea,(err,doc)=>{
                    res(doc);
                });
            }catch(error){
                next(error);
            }
        });
        
    },
    updateTarea: (tarea) => {
        return new Promise((res, reject) => {
            try{
                Tarea.update(Tarea,(err,doc)=>{
                    res(doc);
                });
            }catch(error){
                next(error);
            }
        });
    }
}