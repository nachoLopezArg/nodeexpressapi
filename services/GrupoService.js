import { Grupo } from '../models/mongo/Grupo.js'
import { InexistenteException } from '../models/exceptions/http/InexistenteException.js'
export const GrupoService = {

  createGrupo: (grupo,usuarioActual) => {
    //a user create the group !
    grupo.usuarios = [usuarioActual._id];

    return new Promise((res, reject) => {
        Grupo.create(grupo, (err, doc) => {
          res(doc);
        });
    });

  },
  getGrupoById: (id) => {
    return new Promise((res, reject) => {
      Grupo.findOne({ _id: id }, (error, doc) => {
        if (error) {
          reject(new InexistenteException(`Grupo con el id '${id}' inexistente. `, null, null));
        } else {
          res(doc);
        }
      })
    })
  },
  getGrupos: () => {
    return new Promise((res, reject) => {
      try {
        Grupo.find({}, (error, doc) => {
          res(doc);
        })
      } catch (error) {
        reject(error);
      }

    })
  },
  updateGrupo: (grupo) => {
    return new Promise((res, reject) => {
      try {
        Grupo.update(grupo, (err, doc) => {
          res(doc);
        });
      } catch (error) {
        reject(error);
      }
    });
  },
  deleteGrupo: (id) => {
    return new Promise((res, reject) => {
      try {
        Grupo.deleteOne({ _id: id }, (err, doc) => {
          res(doc);
        });
      } catch (error) {
        reject(error);
      }
    });
  }
}