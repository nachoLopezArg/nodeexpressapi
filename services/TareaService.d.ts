import { Tarea } from "../models/mongo/Tarea";

export module TareaService{
    export function createTarea(Tarea:tarea):Promise<Tarea>;
    export function updateTarea(Tarea:tarea):Promise<Tarea>;
}