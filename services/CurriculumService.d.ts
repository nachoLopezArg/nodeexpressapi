import { Curriculum } from "../models/mongo/Curriculum"

export module CurriculumService {
    export function getCurriculumById(id:String):Promise<Curriculum>;
    export function createCurriculum():Curriculum;
    export function updateCurriculum(): void;
    export function existsCurriculumByEmail(email:String):Promise<Boolean>;
}