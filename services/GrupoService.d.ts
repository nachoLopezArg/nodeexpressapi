import { Grupo } from "../models/mongo/Grupo";
import { User } from "../models/mongo/User";

export module GrupoService{
    export function createGrupo(grupo:Grupo,usuarioActual:User):Promise<Grupo>;
    export function updateGrupo(grupo:Grupo):Promise<Grupo>;
    export function getGrupoById(id:String):Promise<Grupo>;
    export function getGrupos():Promise<Grupo[]>;
    export function deleteGrupo(id:number):void;
}