import express from 'express';
import session from 'express-session';
import cookieParser from 'cookie-parser';
import { errorHandlerMiddleware } from './helpers/errorHandling/ErrorHandlerModule.js';
import { CurriculumRouter } from './routes/CurriculumRouter.js';
import { GrupoRouter } from './routes/GrupoRouter.js';
import { TwitterRouter } from './routes/TwitterRouter.js';
import swaggerUi from 'swagger-ui-express';
import fs from 'fs';
import './helpers/errorHandling/Oauth2AuthenticationMiddleware.js';
import { myPassport, passportRouter, isAuthenticated } from './helpers/errorHandling/Oauth2AuthenticationMiddleware.js';
export const app = express();

const rawDocConfigJson = fs.readFileSync('doc.json');
const swaggerConfig = JSON.parse(rawDocConfigJson);
//Configs
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({
  saveUninitialized: true,
  resave: true,
  secret: "This is a secret"
}));
app.use(myPassport.initialize());
app.use(passportRouter);

//app.use(authenticationMiddleware('/ap i'));

//Api Routes 
app.use(express.static('./resources/static'));
app.use('/docs', isAuthenticated, swaggerUi.serve, swaggerUi.setup(swaggerConfig, { customSiteTitle: "V1.0.0 API" }));
app.use('/api', isAuthenticated, CurriculumRouter, GrupoRouter, TwitterRouter);


//Handlers
app.use(errorHandlerMiddleware);


